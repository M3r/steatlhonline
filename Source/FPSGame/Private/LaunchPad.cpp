// Fill out your copyright notice in the Description page of Project Settings.


#include "LaunchPad.h"

#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "Components/DecalComponent.h"
#include "FPSCharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystem.h"

// Sets default values
ALaunchPad::ALaunchPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
    RootComponent = MeshComponent;

    BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Collider"));
    BoxCollider->SetBoxExtent(FVector(0.5f, 0.5f, 5.0f));
    BoxCollider->SetupAttachment(MeshComponent);

    BoxCollider->OnComponentBeginOverlap.AddDynamic(this, &ALaunchPad::HandleOverlap);

    DecalArrow = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal Arrow"));
    DecalArrow->SetupAttachment(MeshComponent);
   
}

// Called when the game starts or when spawned
void ALaunchPad::BeginPlay()
{
	Super::BeginPlay();
	
}

void ALaunchPad::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    FRotator PlatformFacingDirection = GetActorRotation();
    PlatformFacingDirection.Pitch += LaunchPitchAngle;
    FVector LaunchVelocity = PlatformFacingDirection.Vector() * LaunchForce;

    AFPSCharacter* MyPawn = Cast<AFPSCharacter>(OtherActor);

    if (MyPawn)
    {
        MyPawn->LaunchCharacter(LaunchVelocity, true, true);
    }
    else if (OtherComp && OtherComp->IsSimulatingPhysics())
    {
        OtherComp->AddImpulse(LaunchVelocity, NAME_None, true);
    }
    else
    {
        return;
    }

    if (!ActivateEffect)
    {
        UE_LOG(LogTemp, Warning, TEXT("Particle effect not assigned to launch pad!"));
        return;
    }

    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ActivateEffect, GetActorLocation());
}

// Called every frame
void ALaunchPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

