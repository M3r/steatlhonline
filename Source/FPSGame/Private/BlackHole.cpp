// Fill out your copyright notice in the Description page of Project Settings.


#include "BlackHole.h"

#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABlackHole::ABlackHole()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    BlackHoleMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh Component");
    BlackHoleMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    RootComponent = BlackHoleMesh;
    
    BlackHoleCore = CreateDefaultSubobject<USphereComponent>("Core Sphere Collision Component");
    BlackHoleCore->AttachToComponent(BlackHoleMesh, FAttachmentTransformRules::KeepRelativeTransform);
   
    BlackHoleGravity = CreateDefaultSubobject<USphereComponent>("Gravity Sphere Collision Component");
    BlackHoleGravity->AttachToComponent(BlackHoleMesh, FAttachmentTransformRules::KeepRelativeTransform);

    WhiteParticles = CreateDefaultSubobject<UParticleSystemComponent>("WhiteParticles");
    WhiteParticles->AttachToComponent(BlackHoleMesh, FAttachmentTransformRules::KeepRelativeTransform);
  
    DarkParticles = CreateDefaultSubobject<UParticleSystemComponent>("DarkParticles");
    DarkParticles->AttachToComponent(BlackHoleMesh, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void ABlackHole::BeginPlay()
{
	Super::BeginPlay();

    BlackHoleCore->OnComponentBeginOverlap.AddDynamic(this, &ABlackHole::DestroyOverlappingActor);
	
}

void ABlackHole::DestroyOverlappingActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (OtherActor)
    {
        OtherActor->Destroy();
    }
}

// Called every frame
void ABlackHole::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    Attract();

    // Shrink Black Hole over time
    if (BlackHoleMesh)
    {
        FVector CurrentMeshScale, NewMeshScale;
        CurrentMeshScale = BlackHoleMesh->GetComponentScale();
        if (CurrentMeshScale.X > 0.01f)
        {
            NewMeshScale = CurrentMeshScale - 0.003f;
            BlackHoleMesh->SetWorldScale3D(NewMeshScale);
        }
        else
        {
            // Destroy and play particles

            BlackHoleMesh->GetOwner()->Destroy();
        }
    }
}

void ABlackHole::Attract()
{
    TArray<UPrimitiveComponent*> OverlappingActors;
    BlackHoleGravity->GetOverlappingComponents(OverlappingActors);

    for (UPrimitiveComponent* OverlappingActor : OverlappingActors)
    {
        OverlappingActor->AddRadialForce(GetActorLocation(), GravityRangeRadious, AttractForce, ERadialImpulseFalloff::RIF_Linear, true);
    }
}

