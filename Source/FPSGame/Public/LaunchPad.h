// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LaunchPad.generated.h"

class UStaticMeshComponent;
class UBoxComponent;
class UDecalComponent;
class UParticleSystem;

UCLASS()
class FPSGAME_API ALaunchPad : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALaunchPad();

    // Called every frame
    virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    UFUNCTION()
    void HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UStaticMeshComponent* MeshComponent;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UBoxComponent* BoxCollider;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UDecalComponent* DecalArrow;

    UPROPERTY(EditAnywhere, Category = "Setup")
    float LaunchForce = 800.0f;

    UPROPERTY(EditAnywhere, Category = "Setup")
    float LaunchPitchAngle = 45.0f;

    UPROPERTY(EditAnywhere, Category = "Setup")
    UParticleSystem* ActivateEffect = nullptr;

};
