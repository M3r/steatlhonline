// Fill out your copyright notice in the Description page of Project Settings.

#include "AIDwarfGuard.h"

#include "Runtime/AIModule/Classes/Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "FPSGameMode.h"


// Sets default values
AAIDwarfGuard::AAIDwarfGuard()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    SensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("Senses Component"));

    SensingComponent->OnSeePawn.AddDynamic(this, &AAIDwarfGuard::OnPawnSeen);
    SensingComponent->OnHearNoise.AddDynamic(this, &AAIDwarfGuard::OnNoiseHeard);

    GuardState = EAIState::Idle;
}

// Called when the game starts or when spawned
void AAIDwarfGuard::BeginPlay()
{
	Super::BeginPlay();

    OriginalRotation = GetActorRotation();
	
}

void AAIDwarfGuard::OnPawnSeen(APawn* PawnSeen)
{
    if (!PawnSeen) { return; }

    // Debug draw
    DrawDebugSphere(GetWorld(), PawnSeen->GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);

    // Debug log
    FString SpottedLogText = GetName() + " has spotted the " + PawnSeen->GetName();
    UE_LOG(LogTemp, Warning, TEXT("%s"), *SpottedLogText);
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, SpottedLogText);

    // Fail mission if player seen
    AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
    if (GM)
    {
        GM->CompleteMission(PawnSeen, false);
    }

    SetGuardState(EAIState::Alerted);
}

void AAIDwarfGuard::OnNoiseHeard(APawn* NoiseInstigator, const FVector& Location, float Volume)
{
    if (GuardState == EAIState::Alerted) return;

    // Debug draw
    DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Blue, false, 10.0f);

    // Debug Log
    FString HeardLogText = GetName() + " has head something at " + Location.ToString();
    UE_LOG(LogTemp, Warning, TEXT("%s"), *HeardLogText);
    GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, HeardLogText);

    // Rotate a guard towards noise
    FVector Direction = Location - GetActorLocation();
    Direction.Normalize();

    FRotator NewLookAt = Direction.Rotation();
    NewLookAt.Pitch = 0.0f;
    NewLookAt.Roll = 0.0f;

    SetActorRotation(NewLookAt);

    // Rotate back to original position after some time
    GetWorldTimerManager().ClearTimer(TimerHandle_ResetOrientation);
    GetWorldTimerManager().SetTimer(TimerHandle_ResetOrientation, this, &AAIDwarfGuard::ResetOrientation, 3.0f);

    SetGuardState(EAIState::Suspicious);
}

void AAIDwarfGuard::ResetOrientation()
{
    if (GuardState == EAIState::Alerted) return;

    SetActorRotation(OriginalRotation);

    SetGuardState(EAIState::Idle);
}

void AAIDwarfGuard::SetGuardState(EAIState NewState)
{
    if (GuardState == NewState)
    {
        return;
    }

    GuardState = NewState;

    OnStateChanged(GuardState);
}

// Called every frame
void AAIDwarfGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

