// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Objective.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class UParticleSystem;

UCLASS()
class FPSGAME_API AObjective : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObjective();

protected:

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UStaticMeshComponent* MeshComponent = nullptr;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    USphereComponent* SphereCollisionComponent = nullptr;

    UPROPERTY(EditDefaultsOnly, Category = "FX")
    UParticleSystem* PickupFX;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    void PlayEffects();

public:	

    virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

};
