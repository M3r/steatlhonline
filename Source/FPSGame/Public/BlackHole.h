// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BlackHole.generated.h"

class USphereComponent;
class UParticleSystem;
class UParticleSystemComponent;

UCLASS()
class FPSGAME_API ABlackHole : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlackHole();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

    UFUNCTION()
    void DestroyOverlappingActor(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


    UPROPERTY(VisibleAnywhere, Category = "Params")
    UStaticMeshComponent* BlackHoleMesh;

    UPROPERTY(VisibleAnywhere, Category = "Params")
    USphereComponent* BlackHoleCore;

    UPROPERTY(VisibleAnywhere, Category = "Params")
    USphereComponent* BlackHoleGravity;

    UPROPERTY(VisibleAnywhere, Category = "Params")
    float CoreRadious = 100.0f;

    UPROPERTY(VisibleAnywhere, Category = "Params")
    float GravityRangeRadious = 3000.0f;

    // Positive values push objects away
    UPROPERTY(VisibleAnywhere, Category = "Params")
    float AttractForce = -5000.0f;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UParticleSystemComponent* WhiteParticles = nullptr;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UParticleSystemComponent* DarkParticles = nullptr;

    /*
    UPROPERTY(EditAnywhere, Category = "Setup")
    UParticleSystemComponent* DarkParticlesExplosion = nullptr;

    UPROPERTY(EditAnywhere, Category = "Setup")
    UParticleSystemComponent* WhiteParticlesExplosion = nullptr;

    UPROPERTY(EditAnywhere, Category = "Setup")
    UParticleSystemComponent* DarkParticles = nullptr;
    */ 

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    void Attract();

};
