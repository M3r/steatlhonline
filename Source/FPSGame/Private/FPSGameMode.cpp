// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"

#include "FPSHUD.h"
#include "FPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();
}

void AFPSGameMode::CompleteMission(APawn* InstigatorPawn, bool bIsMissionCompleted)
{
    if (InstigatorPawn)
    {
        InstigatorPawn->DisableInput(nullptr);
        
        if (EndViewpointClass)
        {
            // Point camera at end point
            TArray<AActor*> ReturnedActors;
            UGameplayStatics::GetAllActorsOfClass(this, EndViewpointClass, ReturnedActors);

            if (ReturnedActors.Num() > 0)
            {
                AActor* NewViewTarget = ReturnedActors[0];

                APlayerController* PC = Cast<APlayerController>(InstigatorPawn->GetController());
                if (PC)
                {
                    PC->SetViewTargetWithBlend(NewViewTarget, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
                }
            }
            else
            {
                UE_LOG(LogTemp, Warning, TEXT("End Viewpoint is missing. There won't be camera transition on level completion. Please add BP_EndPoint actor to your level."))
            }
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("EndViewpointClass is nullptr. Please update GameMode class with valid subclass. Cannot change end game view."))
        }
    }

    OnMissionCompleted(InstigatorPawn, bIsMissionCompleted);
}
