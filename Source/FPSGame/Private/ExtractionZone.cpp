// Fill out your copyright notice in the Description page of Project Settings.


#include "ExtractionZone.h"

#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "FPSCharacter.h"
#include "FPSGameMode.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values
AExtractionZone::AExtractionZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

    OverlapBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Overlap Box Component"));
    OverlapBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    OverlapBox->SetCollisionResponseToAllChannels(ECR_Ignore);
    OverlapBox->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
    OverlapBox->SetBoxExtent(FVector(200.0f));
    RootComponent = OverlapBox;

    OverlapBox->OnComponentBeginOverlap.AddDynamic(this, &AExtractionZone::HandleOverlap);

    DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal Component"));
    DecalComponent->SetupAttachment(RootComponent);
    DecalComponent->DecalSize = FVector(200.0f, 200.0f, 200.0f);
}

// Called when the game starts or when spawned
void AExtractionZone::BeginPlay()
{
	Super::BeginPlay();
	
}

void AExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    AFPSCharacter* MyPawn = Cast<AFPSCharacter>(OtherActor);
    
    if (!MyPawn) { return; }

    if (MyPawn->bIsCarryingObjective)
    {
       AFPSGameMode* GM = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
       if (GM)
       {
           GM->CompleteMission(MyPawn, true);
       }
    }
    else
    {
        UGameplayStatics::PlaySound2D(this, ObjectiveNotCollectedSound);
    }
}
