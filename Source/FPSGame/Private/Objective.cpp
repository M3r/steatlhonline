// Fill out your copyright notice in the Description page of Project Settings.

#include "Objective.h"

#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "FPSCharacter.h"

// Sets default values
AObjective::AObjective()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
    MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    RootComponent = MeshComponent;

    SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Collision Component"));
    SphereCollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    SphereCollisionComponent->SetCollisionResponseToAllChannels(ECR_Ignore);
    SphereCollisionComponent->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
    SphereCollisionComponent->SetupAttachment(MeshComponent);

}

// Called when the game starts or when spawned
void AObjective::BeginPlay()
{
	Super::BeginPlay();

}


void AObjective::NotifyActorBeginOverlap(AActor* OtherActor)
{
    Super::NotifyActorBeginOverlap(OtherActor);

    AFPSCharacter* MyCharacter = Cast<AFPSCharacter>(OtherActor);
    if (MyCharacter)
    {
        PlayEffects();

        Destroy();

        MyCharacter->bIsCarryingObjective = true;

        UE_LOG(LogTemp, Warning, TEXT("Objective picked up: %s"), MyCharacter->bIsCarryingObjective ? TEXT("True") : TEXT("False"));
    }
    
}


void AObjective::PlayEffects()
{
    UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());

}

